<?php


if (!function_exists('wordpressblog_logo')){
	function wordpressblog_logo(){
	?>
	<div class="site-name">
		<?php
			if(is_home()){
				printf(
					'<h1><a href="%1$s", title="%2$s">%3$s</a></h1>',
					get_bloginfo('url'),
					get_bloginfo('description'),
					get_bloginfo('sitename')
				);
			}
			else{
				printf(
					'<p>a href="%1$s" title="%2$s">%3$s</a></p>',
					get_bloginfo('url'),
					get_bloginfo('description'),
					get_bloginfo('sitename')
				);
			}?>
			</div>

			<div class="site-description">
				<?php bloginfo('description'); ?>
			</div>
			</div>
			<?php
		}
	}
?>  <!--logo-->



<?php

if ( ! function_exists( 'thachpham_theme_setup' ) ) {
        /*
         * Nếu chưa có hàm thachpham_theme_setup() thì sẽ tạo mới hàm đó
         */
        function thachpham_theme_setup() {
 
        }
        add_action ( 'init', 'thachpham_theme_setup' );
 
  }


  $language_folder = THEME_URL . '/languages';
	load_theme_textdomain( 'thachpham', $language_folder );


	add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-formats',
    array(
       'image',
       'video',
       'gallery',
       'quote',
       'link'
    )
 );

$default_background = array(
   'default-color' => '#e8e8e8',
);
add_theme_support( 'custom-background', $default_background );
?>
<?php
/**
@ blog_menu( $slug )
**/
if ( ! function_exists( 'blog_menu' ) ) {
  function blog_menu( $slug ) {
    $menu = array(
      'theme_location' => $slug,
      'container' => 'nav',
      'container_class' => $slug,
    );
    wp_nav_menu( $menu );
  }
}

/*
* Tạo sidebar cho theme
*/
$sidebar=array(
    'name' => __('Main Sidebar', 'wordpressblog'),
    'id' => 'main-sidebar',
    'description' => 'Main sidebar for wordpressblog',
    'class' => 'main-sidebar',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>'
);
register_sidebar( $sidebar );

?>
<?php



   if ( ! isset( $content_width ) ) {
       /*
        * N?u bi?n $content_width ch?a c? d? li?u th? g?n gi? tr? cho n?
        */
       $content_width = 620;
  }

	$language_folder = THEME_URL . '/languages';
   load_theme_textdomain( 'wordpressblog', $language_folder );




?>
<?php
function bcd_comment($comment, $args, $depth)    {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class();?> id="li-comment-<?php comment_ID();?>">
     
        <div id="comment-<?php comment_ID();?>" class="clearfix comment-body">
            <div class="comment-author vcard">
                <?php echo get_avatar($comment, $size='60', $default='<path_to_url>'); ?>
                <?php printf(__('<span class="fn">%s</span><br />'), get_comment_author_link()); ?>
                <?php if($comment->comment_approved == '0') : ?>
                <em><?php echo 'Your coment is waiting for moderation.';?></em>
                <?php endif; ?>
            </div>
            <div class="comment-meta commentmetadata">
            <?php printf(get_comment_date());?><?php edit_comment_link(__('(Edit)'),' ',''); ?>
            </div>
            <p class="commentcontent"><?php comment_text(); ?></p>
            <div class="reply">
                <?php comment_reply_link(array_merge($args,array('depth' => $depth, 'max_depth'=> $args['max_depth'])));?>
            </div>
        </div>
<?php }?>






<?php
if ( ! function_exists( 'thachpham_thumbnail' ) ) {
  function thachpham_thumbnail( $size ) {
 
    // Chỉ hiển thumbnail với post không có mật khẩu
    if ( ! is_single() &&  has_post_thumbnail()  && ! post_password_required() || has_post_format( 'image' ) ) : ?>
      <figure class="post-thumbnail"><?php the_post_thumbnail( $size ); ?></figure><?php
    endif;
  }
}

/**
@ Hàm hiển thị tiêu đề của post trong .entry-header
@ Tiêu đề của post sẽ là nằm trong thẻ <h1> ở trang single
@ Còn ở trang chủ và trang lưu trữ, nó sẽ là thẻ <h2>
@ thachpham_entry_header()
**/
if ( ! function_exists( 'thachpham_entry_header' ) ) {
  function thachpham_entry_header() {
    if ( is_single() ) : ?>
 
      <h1 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h1>
    <?php else : ?>
      <h2 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h2><?php
 
    endif;
  }
}
  /**
@ Hàm hiển thị thông tin của post (Post Meta)
@ thachpham_entry_meta()
**/
if( ! function_exists( 'thachpham_entry_meta' ) ) {
  function thachpham_entry_meta() {
    if ( ! is_page() ) :
      echo '<div class="entry-meta">';
 
        // Hiển thị tên tác giả, tên category và ngày tháng đăng bài
        printf( __('<span class="author">Posted by %1$s</span>', 'thachpham'),
          get_the_author() );
 
        printf( __('<span class="date-published"> at %1$s</span>', 'thachpham'),
          get_the_date() );
 
        printf( __('<span class="category"> in %1$s</span>', 'thachpham'),
          get_the_category_list( ', ' ) );
 
        // Hiển thị số đếm lượt bình luận
        if ( comments_open() ) :
          echo ' <span class="meta-reply">';
            comments_popup_link(
              __('Leave a comment', 'thachpham'),
              __('One comment', 'thachpham'),
              __('% comments', 'thachpham'),
              __('Read all comments', 'thachpham')
             );
          echo '</span>';
        endif;
      echo '</div>';
    endif;
  }
}
/*
 * Thêm chữ Read More vào excerpt
 */
function thachpham_readmore() {
  return '...<a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'thachpham') . '</a>';
}
add_filter( 'excerpt_more', 'thachpham_readmore' );
 
/**
@ Hàm hiển thị nội dung của post type
@ Hàm này sẽ hiển thị đoạn rút gọn của post ngoài trang chủ (the_excerpt)
@ Nhưng nó sẽ hiển thị toàn bộ nội dung của post ở trang single (the_content)
@ thachpham_entry_content()
**/
if ( ! function_exists( 'thachpham_entry_content' ) ) {
  function thachpham_entry_content() {
 
    if ( ! is_single() ) :
      the_excerpt();
    else :
      the_content();
 
      /*
       * Code hiển thị phân trang trong post type
       */
      $link_pages = array(
        'before' => __('<p>Page:', 'thachpham'),
        'after' => '</p>',
        'nextpagelink'     => __( 'Next page', 'thachpham' ),
        'previouspagelink' => __( 'Previous page', 'thachpham' )
      );
      wp_link_pages( $link_pages );
    endif;
 
  }
}
/**
@ Hàm hiển thị tag của post
@ thachpham_entry_tag()
**/
if ( ! function_exists( 'thachpham_entry_tag' ) ) {
  function thachpham_entry_tag() {
    if ( has_tag() ):
      echo '<div class="entry-tag">';
      printf( __('Tagged in %1$s', 'thachpham'), get_the_tag_list( '', ', ' ) );
      echo '</div>';
    endif;
  }
}

add_theme_support( 'post-formats',
    array(
       'image',
       'video',
       'gallery',
       'quote',
       'link'
    )
 );

?>
<?php
    session_start();
    $username="user";
    $password="password";

    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){
        header("location: index.php");
    }
    if(isset($_POST['username']) && isset($_POST['password'])){
        if($_POST['username'] == $username && $_POST['password'] == $password){
            $_SESSION['loggedin']=true;
            header("location:index.php");
        }
    }

