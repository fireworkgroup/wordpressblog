<?php get_header(); ?>

<div id="content">
    <div id="searchResults">       
        <h2>Result search</h2>
        <?php if(have_posts()) : while(have_posts()) : the_post() ?>
        <div class="search">
            <h1><?php the_title();?></h1>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/timthumb.php?src=<?php echo get_post_meta($post->ID,'Thumb',true)?>&a=t&w=130&h=130" border="0" />
                <?php the_content_rss('', false,'', 40);?>
            </p>
        </div>
            <?php endwhile; else: ?>
            <p>Can not find! :(</p>
            <?php endif; ?>
    </div>

</div>
<?php get_footer(); ?>
