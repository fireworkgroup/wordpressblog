<?php

get_header();?>
<div id="container">
    <div id="content" role="main">
        <?php the_post();?>
        <h1><?php the_title();?></h1>
        <?php get_seach_form();?>
        <h2> Archive by month:</h2>
        <ul>
             <?php wp_get_archives('type=monthly'); ?>
        </ul>

        <ul>
            <?php wp_list_categories();?>
        </ul>
    </div>
</div>
<?php get_sider();?>
